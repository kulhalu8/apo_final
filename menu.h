//
// Created by root on 5/16/18.
//

#ifndef UNTITLED_MENU_H
#define UNTITLED_MENU_H

#include <stdbool.h>


/**
 * @brief Option in a menu
 */
typedef struct {
    char *name;
    double max_value;
    double min_value;
    volatile void *var;
    bool selected;
    double step;
} option_t;

/**
 * @brief Menu structure to make life a little easier
 */
typedef struct {
    option_t *options;
    int selected;
    int size;
} menu_t;

menu_t current_menu;

void render_menu();

void build_info_menu();

void red_right();
void red_left();
void green_right();
void green_left();
void blue_right();
void blue_left();
void red_click();
void green_click();
void blue_click();

#endif //UNTITLED_MENU_H
