#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <arpa/inet.h>
#include <sys/socket.h>
#include <unistd.h>
#include "settings.h"
#include "main.h"

#define BUFLEN 512
#define PORT 44444


/**
 * @brief Main loop for receiving network commands
 * @param args Optional arguments passed when thread gets started
 */
void *network_run(void * args)
{
    struct sockaddr_in si_me, si_other;

    int s, slen = sizeof(si_other) , recv_len;
    char buf[BUFLEN];

    if ((s=socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP)) == -1)
    {
        printf("[network] Couldn't create socket\n");
    }

    struct timeval read_timeout;
    read_timeout.tv_sec = 0;
    read_timeout.tv_usec = 1000;
    setsockopt(s, SOL_SOCKET, SO_RCVTIMEO, &read_timeout, sizeof read_timeout);

    memset((char *) &si_me, 0, sizeof(si_me));

    si_me.sin_family = AF_INET;
    si_me.sin_port = htons(PORT);
    si_me.sin_addr.s_addr = htonl(INADDR_ANY);

    if( bind(s , (struct sockaddr*)&si_me, sizeof(si_me) ) == -1)
    {
        printf("[network] Couldn't bind to port %d\n", PORT);
    }
#ifdef DEBUG
	printf("[network] Listening for data on port %d\n", PORT);
#endif
    //keep listening for data
    while(!appEnd) {
        recv_len = recvfrom(s, buf, BUFLEN, 0, (struct sockaddr *) &si_other, (socklen_t *)&slen);
        if (recv_len != -1)
        {
#ifdef DEBUG
                printf("[network] Received command from %s:%d\n", inet_ntoa(si_other.sin_addr), ntohs(si_other.sin_port));
#endif
            double argument;
            settings_t setting;
            char command;

            int converted = sscanf(buf, "%c %d %lf", &command, (int *)&setting, (double *)&argument);
            if (command == 'S' && converted == 3) {
                change_setting(setting, argument);
            } else if (command == 'M')
                swap_rendered();
            else if (command == 'C')
                settings_commit();
        }
    }

    close(s);
#ifdef DEBUG
    printf("[network] Thread end\n");
#endif
    return NULL;
}
