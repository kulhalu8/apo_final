//
// Created by root on 5/16/18.
//

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "render.h"
#include "julia.h"
#include "settings.h"
#include "main.h"
#include "menu.h"

/**
 * @brief Main loop for calculating julia set
 * @param args Optional arguments passed when thread gets started
 */
void julia(void * args) {
    double real;
    double image;
    double tmp_real;
    double tmp_imag;
    RgbColor color;
    HsvColor clr;

    while ((rendered == JULIA || rendered == JULIA_SLIDESHOW) && appEnd == false) {
#ifdef DEBUG
        printf("[julia] Acquiring settings lock\n");
#endif
        pthread_mutex_lock(&settingsLock);
        double shift_x = julia_settings.shift_x;
        double shift_y = julia_settings.shift_y;
        double zoom = julia_settings.zoom;
        double imaginary_const = julia_settings.imaginary_constant;
        double real_const = julia_settings.real_constant;
        double max_iter = julia_settings.max_iterations;

        int max_hue = render_settings.max_hue;
        int max_value = render_settings.max_value;
        int max_saturation = render_settings.max_saturation;
#ifdef DEBUG
        printf("[julia] Letting go of settings lock\n");
#endif
        pthread_mutex_unlock(&settingsLock);

        FB_TYPE * tmpfb = get_fb_copy(fb);
#ifdef DEBUG
        printf("[julia] Rendering new\n");
#endif
        for (int y = 0; y < HEIGHT; y++)
            for (int x = 0; x < WIDTH; x++) {
                real = 1.5 * (x - WIDTH / 2) / (0.5 * zoom * WIDTH) + shift_x;
                image = (y - HEIGHT / 2) / (0.5 * zoom* HEIGHT) + shift_y;
                int i;
                for (i = 0; i < max_iter; i++) {
                    tmp_real = real;
                    tmp_imag = image;
                    real = tmp_real * tmp_real - tmp_imag * tmp_imag + real_const;
                    image = 2 * tmp_real * tmp_imag + imaginary_const;
                    if ((real * real + image * image) > 16) break;
                }
                clr.h = i % max_hue;
                clr.s = max_saturation;
                clr.v = max_value * (i < max_iter);
                color = HsvToRgb(clr);
                set_pixel_fb(tmpfb, x, y, color);
            }

#ifdef DEBUG
        printf("[julia] Acquiring rendering lock\n");
#endif
        pthread_mutex_lock(&rendererLock);
        free(fb);
        fb = tmpfb;
#ifdef DEBUG
        printf("[julia] Letting go of rendering lock\n");
#endif
        if (drawOverlay) {
            RgbColor COLOR_WHITE;
            COLOR_WHITE.r = 255;
            COLOR_WHITE.g = 255;
            COLOR_WHITE.b = 255;

            if (current_menu.options == NULL)
                build_info_menu();

            for (int i = 0; i < 5; ++i) {
                option_t opt = current_menu.options[i];
                char output[32];
                snprintf(output, 32, "%.2f", *(double *)(opt.var));
                char tmpbuf[strlen(output) + strlen(opt.name)];
                strcpy((char *)&tmpbuf, opt.name);
                strcat((char *)&tmpbuf, output);

                draw_string(tmpbuf, 0,  (16 * i), COLOR_WHITE);
            }
        }
        pthread_mutex_unlock(&rendererLock);


#ifdef DEBUG
        printf("[julia] Waiting for settings change\n");
#endif
        pthread_cond_signal(&fbChanged);
        if (rendered == JULIA_SLIDESHOW) {
            slideshow_next();
        }
        else {
            pthread_cond_wait(&settingsChanged, &settingsLock);
            pthread_mutex_unlock(&settingsLock);
        }
    }
    pthread_mutex_unlock(&settingsLock);
}

/**
 * @brief Main loop for managing what gets displayed on the screen
 */
void *redraw_run(void * args) {
    if (rendered == JULIA && !appEnd) {
#ifdef DEBUG
        printf("[switch] Rendering julia\n");
#endif
        julia(NULL);
    }
    else if (!appEnd) {
#ifdef DEBUG
        printf("[switch] Rendering menu\n");
#endif
        render_menu();
#ifdef DEBUG
        printf("[switch] Rendering menu\n");
#endif
    }

    return NULL;
}
