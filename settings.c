//
// Created by root on 5/16/18.
//

#include "settings.h"
#include "main.h"
#include "menu.h"
#include <stdio.h>
#include <unistd.h>
/**
 * @brief Global variable holding julia settings
 */
volatile julia_settings_t julia_settings;
/**
 * @brief Global variable holding render settings
 */
render_settings_t render_settings;

volatile bool drawOverlay = true;

/**
 * @brief Initializes settings to default values
 */
void init_settings() {
    julia_settings.real_constant = -0.68;
    julia_settings.imaginary_constant = 0.32;
    julia_settings.shift_x = 0;
    julia_settings.shift_y = 0;
    julia_settings.zoom = 1;
    julia_settings.max_iterations = 600.0;

    render_settings.max_hue = 256;
    render_settings.max_saturation = 255;
    render_settings.max_value = 255;
}

/**
 * @brief Changes a given setting to a given value, if the setting exists
 * @param setting Setting to change
 * @param argument Value
 */
void change_setting(settings_t setting, double argument) {
    pthread_mutex_lock(&settingsLock);
    if (setting == JULIA_REAL)
        julia_settings.real_constant = argument;
    else if (setting == JULIA_IMAGINARY)
        julia_settings.imaginary_constant = argument;
    else if (setting == JULIA_ZOOM)
        julia_settings.zoom = argument;
    else if (setting == JULIA_SHIFT_X)
        julia_settings.shift_x = argument;
    else if (setting == JULIA_SHIFT_Y)
        julia_settings.shift_y = argument;
    else if (setting == JULIA_MAX_ITER)
        julia_settings.max_iterations = argument;
    else if (setting == RED_RIGHT)
        red_right();
    else if (setting == RED_LEFT)
        red_left();
    else if (setting == GREEN_RIGHT)
        green_right();
    else if(setting == GREEN_LEFT)
        green_left();
    else if (setting == GREEN_CLICK)
        green_click();
    else if(setting == RENDER_MAX_HUE)
        render_settings.max_hue = argument;
    else if(setting == RENDER_MAX_SATURATION)
        render_settings.max_saturation = argument;
    else if(setting == RENDER_MAX_VALUE)
        render_settings.max_value = argument;

    pthread_mutex_unlock(&settingsLock);
}

/**
 * @brief Flip-flops what gets rendered on the screen
 */
void swap_rendered() {
    if (rendered == MENU_INFO)
        rendered = JULIA;
    else if (rendered == JULIA)
        rendered = MENU_INFO;
#ifdef DEBUG
    printf("[settings] Rendered changed to %d\n", rendered);
#endif
    settings_commit();
}

/**
 * @brief Notify threads about settings change
 */
void settings_commit() {
    pthread_cond_signal(&settingsChanged);
}

volatile int slideshowIndex = 0;

/**
 * @brief Replaces current settings with the next settings for slideshow
 */
void slideshow_next() {
    julia_settings_t opts[5];
    int size = 5;

    opts[0].shift_x = 0;
    opts[0].shift_y = 0;
    opts[0].max_iterations = 600;
    opts[0].zoom = 1;
    opts[0].real_constant = 0.33;
    opts[0].imaginary_constant = -0.33;

    opts[1].shift_x = 0;
    opts[1].shift_y = 0;
    opts[1].max_iterations = 600;
    opts[1].zoom = 1;
    opts[1].real_constant = -0.33;
    opts[1].imaginary_constant = 0.79;

    opts[2].shift_x = 0;
    opts[2].shift_y = 0;
    opts[2].max_iterations = 600;
    opts[2].zoom = 1;
    opts[2].real_constant = 0.31;
    opts[2].imaginary_constant = -0.60;

    opts[3].shift_x = 0;
    opts[3].shift_y = 0;
    opts[3].max_iterations = 600;
    opts[3].zoom = 1;
    opts[3].real_constant = 0.33;
    opts[3].imaginary_constant = -0.56;

    opts[4].shift_x = 0;
    opts[4].shift_y = 0;
    opts[4].max_iterations = 600;
    opts[4].zoom = 1;
    opts[4].real_constant = 0.31;
    opts[4].imaginary_constant = -0.60;

    sleep(1);
    slideshowIndex = (++slideshowIndex < size) ? slideshowIndex : 0;
    printf("[julia] Changing to option %d\n", slideshowIndex);
    julia_settings = opts[slideshowIndex];
    settings_commit();
}