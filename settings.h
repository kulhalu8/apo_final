//
// Created by root on 5/16/18.
//

#ifndef UNTITLED_SETTINGS_H
#define UNTITLED_SETTINGS_H

#include <stdbool.h>
/**
 * @brief Holds all variables that are configurable for julia set
 */
typedef struct {
    volatile double real_constant;
    volatile double imaginary_constant;

    volatile double shift_x;
    volatile double shift_y;

    volatile double zoom;
    volatile double max_iterations;
} julia_settings_t;

/**
 * @brief Holds all variables that are configurable for rendering options
 */
typedef struct {
    volatile double max_hue;
    volatile double max_saturation;
    volatile double max_value;
} render_settings_t;

/**
 * @brief Types of stuff that can be drawn on the screen
 */
typedef enum {
    JULIA,
    MENU_INFO,
    JULIA_SLIDESHOW
} renderables_t;

renderables_t rendered;

volatile julia_settings_t julia_settings;
render_settings_t render_settings;

volatile bool drawOverlay;

/***
 * @brief Defines all possible events that can happen
 */
typedef enum  {
    JULIA_REAL,
    JULIA_IMAGINARY,
    JULIA_SHIFT_X,
    JULIA_SHIFT_Y,
    JULIA_ZOOM,
    JULIA_MAX_ITER,
    RENDER_MAX_HUE,
    RENDER_MAX_SATURATION,
    RENDER_MAX_VALUE,
    RED_RIGHT,
    RED_LEFT,
    GREEN_RIGHT,
    GREEN_LEFT,
    BLUE_RIGHT,
    BLUE_LEFT,
    RED_CLICK,
    GREEN_CLICK,
    BLUE_CLICK,
    INVALID
} settings_t;

void init_settings();
void change_setting(settings_t, double);
void swap_rendered();
void settings_commit();
void slideshow_next();

#endif //UNTITLED_SETTINGS_H
