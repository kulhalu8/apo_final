//
// Created by root on 5/16/18.
//

#ifndef UNTITLED_MAIN_H
#define UNTITLED_MAIN_H

#include <pthread.h>
#include <stdbool.h>

pthread_t rendererThread;
pthread_t redrawThread;
pthread_t networkThread;
pthread_t hwThread;

pthread_mutex_t rendererLock;
pthread_mutex_t settingsLock;

volatile bool appEnd;

pthread_cond_t settingsChanged;
pthread_cond_t fbChanged;
pthread_cond_t renderDone;

#endif //UNTITLED_MAIN_H
