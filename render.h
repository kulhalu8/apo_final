//
// Created by root on 5/16/18.
//

#ifndef UNTITLED_RENDER_H
#define UNTITLED_RENDER_H

#include <stdint.h>

#define WIDTH 480
#define HEIGHT 320

#ifdef WITH_SDL
    #define FB_TYPE uint32_t
#else
    #define FB_TYPE uint16_t
#endif

#define FB_SIZE WIDTH * HEIGHT * sizeof(FB_TYPE)

extern FB_TYPE *fb;


typedef struct RgbColor
{
    unsigned char r;
    unsigned char g;
    unsigned char b;
} RgbColor;

typedef struct HsvColor
{
    unsigned char h;
    unsigned char s;
    unsigned char v;
} HsvColor;

RgbColor HsvToRgb(HsvColor);
void set_pixel(int, int, RgbColor);
void set_pixel_fb(FB_TYPE *, int, int, RgbColor);
void *renderer_run(void *args);

FB_TYPE *render_init();
FB_TYPE *get_fb_copy(FB_TYPE*);

void draw_rectangle(int startx, int starty, int endx, int endy, RgbColor color);
void draw_string(char* str, int x, int y, RgbColor color);

void *poll_hw(void *);

#endif //UNTITLED_RENDER_H
