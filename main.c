#define _GNU_SOURCE

#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include "render.h"
#include "network.h"
#include "main.h"
#include "settings.h"
#include "julia.h"
#include "menu.h"

FB_TYPE *fb;
renderables_t rendered = JULIA;
//volatile bool appEnd = false;

/**
 * @brief Helper function to initilaize mutexes
 * @param mt Mutex to initialize
 */
void init_mutex(pthread_mutex_t *mt) {
    if (pthread_mutex_init(mt, NULL) != 0)
    {
        fprintf(stderr, "Couldn't create mutex\n");
        exit(-1);
    }
}

int main(int argc, char ** argv)
{

    appEnd = false;
    current_menu.options = NULL;
    fb = render_init();
    init_settings();

    init_mutex(&rendererLock);
    init_mutex(&settingsLock);

    pthread_cond_init(&fbChanged, NULL);
    pthread_cond_init(&renderDone, NULL);
    pthread_cond_init(&settingsChanged, NULL);

    if(pthread_create(&rendererThread, NULL, renderer_run, NULL)) {
        fprintf(stderr, "Error creating renderer thread\n");
        return 1;
    }
    pthread_setname_np(rendererThread, "apo-renderer");

    if(pthread_create(&networkThread, NULL, network_run, NULL)) {
        fprintf(stderr, "Error creating network thread\n");
        return 1;
    }
    pthread_setname_np(networkThread, "apo-network");

	if(pthread_create(&hwThread, NULL, poll_hw, NULL)) {
        fprintf(stderr, "Error creating network thread\n");
        return 1;
    }
    pthread_setname_np(hwThread, "apo-hw-polling");

    rendered = JULIA;
    while(!appEnd) {
        redraw_run(NULL);
    }

    int *ret;
    pthread_cond_signal(&settingsChanged);
    pthread_cond_signal(&fbChanged);

    pthread_join(networkThread, (void **)&ret);
    pthread_join(rendererThread, (void **)&ret);
    pthread_join(redrawThread, (void **)&ret);
    pthread_join(hwThread, (void **)&ret);

    pthread_mutex_destroy(&rendererLock);
    pthread_mutex_destroy(&settingsLock);
    pthread_cond_destroy(&fbChanged);
    pthread_cond_destroy(&settingsChanged);

    free(current_menu.options);
    //pthread_

    return 0;
}
