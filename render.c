//
// Created by root on 5/16/18.
//
#include "render.h"
#include "main.h"
#include <stdlib.h>
#include <stdbool.h>
#include <pthread.h>
#include <string.h>
#include <time.h>
#include "menu.h"
#include "settings.h"

#include "font.h"
#include "font_types.h"

#ifdef WITH_SDL
    #include <SDL.h>
#else
	#include "mzapo_parlcd.h"
	#include "mzapo_phys.h"
	#include "mzapo_regs.h"
#endif

#ifndef WITH_SDL
	#define round(x) (int)x + 0.5
#endif

/**
 * @brief Allocates and initializes framebuffer array
 * @return  Pointer to framebuffer
 */
FB_TYPE *render_init() {
    FB_TYPE *fbb = (FB_TYPE *)malloc(FB_SIZE);
    memset(fbb, 255, FB_SIZE);
    return fbb;
}

#ifdef WITH_SDL
    /**
     * @brief Sets a specified pixel to a specified color in the framebuffer
     * @param x X coordinate
     * @param y Y coordinate
     * @param color Color to set
     */
    void set_pixel(int x, int y, RgbColor color) {
        fb[x + (y * WIDTH)] = 0 | (color.r << 16) | (color.g << 8) | color.b;
    }
#else
    /**
     * @brief Sets a specified pixel to a specified color in the framebuffer
     * @param x X coordinate
     * @param y Y coordinate
     * @param color Color to set
     */
    void set_pixel(int x, int y, RgbColor color) {
        uint8_t r = round(color.r / 8);
        uint8_t g = round(color.g / 4);
        uint8_t b = round(color.b / 8);

        uint16_t rgb = ((r << 11) & 0xF800) | ((g << 5) & 0x07E0) | (b & 0x001F);
        fb[x + (y * WIDTH)] = rgb;
    }
#endif

#ifdef WITH_SDL
void set_pixel_fb(FB_TYPE* fbc, int x, int y, RgbColor color) {
    fbc[x + (y * WIDTH)] = 0 | (color.r << 24) | (color.g << 16) | color.b;
    //printf("%d\n", fbc[x + (y * WIDTH)]);
}
#else
void set_pixel_fb(FB_TYPE* fbc, int x, int y, RgbColor color) {
        uint8_t r = round(color.r / 8);
        uint8_t g = round(color.g / 4);
        uint8_t b = round(color.b / 8);

        uint16_t rgb = ((r << 11) & 0xF800) | ((g << 5) & 0x07E0) | (b & 0x001F);
        fbc[x + (y * WIDTH)] = rgb;
    }
#endif

/**
 * @brief
 * @param fbsrc
 * @return
 */
FB_TYPE *get_fb_copy(FB_TYPE *fbsrc) {
    FB_TYPE *fbcopy = malloc(FB_SIZE);
    //memcpy(fbcopy, fbsrc, FB_SIZE);

    memset(fbcopy, 255, FB_SIZE);
    return fbcopy;
}

#ifdef WITH_SDL
    /**
     * @brief Initialization of SDL and main loop for rendering thread
     * @param args Optional arguments passed when thread gets started
     */
    void *renderer_run(void *args) {
        SDL_Init(SDL_INIT_VIDEO);
        SDL_Window *window = SDL_CreateWindow("APO semestral project",
                                              SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, WIDTH, HEIGHT, 0);
        SDL_Renderer *renderer = SDL_CreateRenderer(window, -1, 0);
        SDL_Texture *texture = SDL_CreateTexture(renderer,
                                                 SDL_PIXELFORMAT_ARGB8888, SDL_TEXTUREACCESS_STATIC, WIDTH, HEIGHT);
        //pthread_mutex_lock(&rendererLock);
        SDL_Event event;
        while (!appEnd) {
            while (SDL_PollEvent(&event)) {
				SDL_KeyboardEvent ke;
				switch (event.type) {
					case SDL_QUIT:
#ifdef DEBUG
                        printf("[render] SDL_QUIT\n");
#endif
                        appEnd = true;
                        pthread_cond_signal(&settingsChanged);
						break;
					case SDL_KEYUP:
						ke = event.key;
						SDL_Scancode sc = ke.keysym.scancode;

						if (sc == SDL_SCANCODE_M)
							swap_rendered();
						else if (sc == SDL_SCANCODE_E)
							red_left();
						else if (sc == SDL_SCANCODE_T)
							red_right();
						else if (sc == SDL_SCANCODE_F)
							green_left();
						else if (sc == SDL_SCANCODE_H)
							green_right();
						else if (sc == SDL_SCANCODE_V)
							blue_left();
						else if (sc == SDL_SCANCODE_N)
							blue_right();
						else if (sc == SDL_SCANCODE_R)
							red_click();
						else if (sc == SDL_SCANCODE_B)
							blue_click();
						else if (sc == SDL_SCANCODE_G)
							green_click();

						struct timespec ts;
						uint8_t milliseconds = 10;
						ts.tv_sec = milliseconds / 1000;
						ts.tv_nsec = (milliseconds % 1000) * 1000000;
						nanosleep(&ts, NULL);
						break;
				}
            }

            pthread_mutex_lock(&rendererLock);
            SDL_UpdateTexture(texture, NULL, fb, WIDTH * sizeof(uint32_t));
            pthread_mutex_unlock(&rendererLock);

            SDL_RenderClear(renderer);
            SDL_RenderCopy(renderer, texture, NULL, NULL);
            SDL_RenderPresent(renderer);
        }

        free(fb);
        SDL_DestroyTexture(texture);
        SDL_DestroyRenderer(renderer);
        SDL_DestroyWindow(window);
        SDL_Quit();
#ifdef DEBUG
        printf("[render] Thread end\n");
#endif
        return NULL;
    }
#else
	void *renderer_run(void *args) {
		unsigned char *parlcd_mem_base = map_phys_address(PARLCD_REG_BASE_PHYS, PARLCD_REG_SIZE, 0);

		if (parlcd_mem_base == NULL)
			exit(1);

		parlcd_hx8357_init(parlcd_mem_base);

		pthread_mutex_lock(&rendererLock);
		while (!appEnd) {
			parlcd_write_cmd(parlcd_mem_base, 0x2c);
			uint16_t c;
			for (int i = 0; i < HEIGHT ; i++) {
				for (int j = 0; j < WIDTH ; j++) {
					c = fb[(i * WIDTH) + j];
					parlcd_write_data(parlcd_mem_base, c);
				}
			}
			pthread_cond_wait(&fbChanged, &rendererLock);
		}
		return NULL;
	}
#endif

/**
 * @brief Converts color in HSV format to RGB
 * @param hsv Color in HSV format
 * @return Color in RGB format
 */
RgbColor HsvToRgb(HsvColor hsv)
{
    RgbColor rgb;
    unsigned char region, remainder, p, q, t;

    if (hsv.s == 0)
    {
        rgb.r = hsv.v;
        rgb.g = hsv.v;
        rgb.b = hsv.v;
        return rgb;
    }

    region = hsv.h / 43;
    remainder = (hsv.h - (region * 43)) * 6;

    p = (hsv.v * (255 - hsv.s)) >> 8;
    q = (hsv.v * (255 - ((hsv.s * remainder) >> 8))) >> 8;
    t = (hsv.v * (255 - ((hsv.s * (255 - remainder)) >> 8))) >> 8;

    switch (region)
    {
        case 0:
            rgb.r = hsv.v; rgb.g = t; rgb.b = p;
            break;
        case 1:
            rgb.r = q; rgb.g = hsv.v; rgb.b = p;
            break;
        case 2:
            rgb.r = p; rgb.g = hsv.v; rgb.b = t;
            break;
        case 3:
            rgb.r = p; rgb.g = q; rgb.b = hsv.v;
            break;
        case 4:
            rgb.r = t; rgb.g = p; rgb.b = hsv.v;
            break;
        default:
            rgb.r = hsv.v; rgb.g = p; rgb.b = q;
            break;
    }

    return rgb;
}

/**
 * @brief Draws a character at a specified place in a framebuffer
 * @param x X coordinate
 * @param y Y coordinate
 * @param color Character color
 * @param c Character code
 */
void draw_character(int x, int y, RgbColor color, unsigned char c) {
    for (int i = 0; i < 16; ++i)
    {
        uint16_t mask = 0x8000;
        uint16_t charRow = font_rom8x16.bits[(c * 16) + i];

        for (int j = 0; j < 8; ++j) {
            if ((charRow & (mask >> j)) != 0)
                set_pixel(x + j, y, color);
            /*else
                *(dest + j) = 0xFFFFFFFF;*/
        }
        y += 1;
    }
}

/**
 * @brief Draws a string at a specified place in the framebuffer
 * @param str String to draw
 * @param x X coordinate
 * @param y Y coordinate
 * @param color String color
 */
void draw_string(char *str, int x, int y, RgbColor color) {
    for (int i = 0; str[i] != '\0'; ++i) {
        draw_character(x, y, color, str[i]);
        x += 10;
    }
}

/**
 * @brief Draws a rectangle defined by two points
 * @param startx X coordinate of first point
 * @param starty Y coordinate of first point
 * @param endx X coordinate of last point
 * @param endy Y coordinate of last point
 * @param color Color to draw
 */

void draw_rectangle(int startx, int starty, int endx, int endy, RgbColor color) {
    for (int i = starty; i < endy; ++i)
        for (int j = startx; j < endx; ++j)
            set_pixel(j, i, color);
}

#ifdef WITH_SDL
	/**
	 * @brief For SDL controls are handled in the render thread
	 */
    void *poll_hw(void * args) {
        return NULL;
    }
#else
	int get_direction(uint8_t cur, uint8_t prev, int8_t cur_dir) {
		if (cur == 0 && prev == 255)
			return 1;
		else if(cur == 255 && prev == 0)
			return -1;
		else if (cur > prev)
			return 1;
		else if (cur < prev)
			return -1;

		return cur_dir;
	}

	/**
	 * @brief Calculates the number of steps between two values, takes overflowing into account
	 * @param cur Current value
	 * @param prev Previous value
	 * @return Steps between given values
	 */
	int get_steps(uint8_t cur, uint8_t prev) {
		if (cur == 0 && prev == 63)
			return 1;
		else if (prev == 0 && cur == 63)
			return -1; 
		else if (cur > prev)
			return 1;
		else if (prev > cur)
			return -1;
		return 0;
	}

	/**
	 * @brief Main function for HW thread
	 * @param args NULL
	 * @return NULL
	 */
	void *poll_hw(void *args) {
		unsigned char *mem_base;
		mem_base = map_phys_address(SPILED_REG_BASE_PHYS, SPILED_REG_SIZE, 0);
		uint32_t rgb_knobs_value;
		uint8_t r, g, b;			
		uint8_t r_prev = 0, g_prev = 0, b_prev = 0;
		uint8_t r_click, g_click, b_click;
		int8_t r_direction = 0, g_direction = 0, b_direction = 0;
		
		uint8_t r_click_prev = 0, b_click_prev = 0, g_click_prev = 0;

		uint8_t r_steps = 0, g_steps = 0, b_steps = 0;

		while (!appEnd) {
			rgb_knobs_value = *(volatile uint32_t*)(mem_base + SPILED_REG_KNOBS_8BIT_o);	
			r = (rgb_knobs_value & (0xFF << 16)) >> 18;
			r_direction = get_direction(r, r_prev, r_direction);

			g = (rgb_knobs_value & (0xFF << 8)) >> 10;
			g_direction = get_direction(g, g_prev, g_direction);

			b = (rgb_knobs_value & 0xFF) >> 2;			
			b_direction = get_direction(b, b_prev, b_direction);

			r_click = (rgb_knobs_value & (0x01 << 26)) >> 26;
			g_click = (rgb_knobs_value & (0x01 << 25)) >> 25;
			b_click = (rgb_knobs_value & (0x01 << 24)) >> 24;

			if (r_click == 1 && r_click_prev == 0) {
				red_click();
			}

			if (g_click == 1 && g_click_prev == 0) {
				green_click();
			}

			if (b_click == 1 && b_click_prev == 0) {
				blue_click();
			}

			r_steps = get_steps(r, r_prev);
			g_steps = get_steps(g, g_prev);
			b_steps = get_steps(b, b_prev);
			if (r_steps != 0) {
				if (r_steps == 1)
					red_right();
				else 
					red_left();
			}

			if (g_steps != 0) {
				if (g_steps == 1)
					green_right();
				else 
					green_left();
			}
			if (b_steps != 0) {
				if (b_steps == 1)
					blue_right();
				else
					blue_left();
			}

			r_click_prev = r_click;
			g_click_prev = g_click;
			b_click_prev = b_click;
			
			if (r != r_prev)
				r_prev = r;
			if (g != g_prev)
				g_prev = g;
			if (b != b_prev)
				b_prev = b;

		
		}
		return NULL;
	}
#endif
