//
// Created by root on 5/16/18.
//

#include "menu.h"
#include "main.h"
#include "render.h"
#include "settings.h"
#include <stdbool.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

/**
 * @brief Holds the current displayed menu
 */
menu_t current_menu;

/**
 * @brief Initializes hardcoded menu structure
 */
void build_info_menu() {
    option_t *options;
    options = malloc(9 * sizeof(option_t));

    options[0].name = "Julia real: ";
    options[0].step = 0.01;
    options[0].var = &julia_settings.real_constant;
    options[0].min_value = -1.0;
    options[0].max_value = 1.0;

    options[1].name = "Julia imaginary: ";
    options[1].step = 0.01;
    options[1].var = &julia_settings.imaginary_constant;
    options[1].min_value = -1.0;
    options[1].max_value = 1.0;

    options[2].name = "Julia X shift: ";
    options[2].step = 0.1;
    options[2].var = &julia_settings.shift_x;
    options[2].max_value = 99999;
    options[2].min_value = -99999;

    options[3].name = "Julia Y shift: ";
    options[3].step = 0.1;
    options[3].var = &julia_settings.shift_y;
    options[3].max_value = 99999;
    options[3].min_value = -99999;

    options[4].name = "Julia max iterations: ";
    options[4].step = 50;
    options[4].var = &julia_settings.max_iterations;
    options[4].max_value = 99999;
    options[4].min_value = -99999;

    options[5].name = "Julia zoom: ";
    options[5].step = 5;
    options[5].var = &julia_settings.zoom;
    options[5].max_value = 99999;
    options[5].min_value = -99999;

    options[6].name = "Max hue: ";
    options[6].step = 5;
    options[6].var = &render_settings.max_hue;
    options[6].max_value = 255;
    options[6].min_value = 0;

    options[7].name = "Max saturation: ";
    options[7].step = 5;
    options[7].var = &render_settings.max_saturation;
    options[7].max_value = 255;
    options[7].min_value = 0;

    options[8].name = "Max value: ";
    options[8].step = 5;
    options[8].var = &render_settings.max_value;
    options[8].max_value = 255;
    options[8].min_value = 0;

    free(current_menu.options);
    current_menu.options = options;
    current_menu.size = 9;
    current_menu.selected = 0;
}

/**
 * @brief Main loop for rendering menu to framebuffer
 */
void render_menu() {
    if (current_menu.options == NULL)
        build_info_menu();

    RgbColor blue;
    blue.r = 0;
    blue.g = 0;
    blue.b = 255;

    RgbColor COLOR_WHITE;
    COLOR_WHITE.r = 255;
    COLOR_WHITE.g = 255;
    COLOR_WHITE.b = 255;

    drawOverlay = false;

    while (rendered == MENU_INFO && appEnd == false) {
        draw_rectangle(0,0,WIDTH,HEIGHT,blue);

        for (int i = 0; i < current_menu.size; ++i) {
            option_t opt = current_menu.options[i];
            char output[32];
            snprintf(output, 32, "%.2f", *(double *)(opt.var));
            char tmpbuf[strlen(output) + strlen(opt.name)];
            strcpy((char *)&tmpbuf, opt.name);
            strcat((char *)&tmpbuf, output);
            if (current_menu.selected == i)
            {
                draw_rectangle(20, 20 + 20 * i, WIDTH - 20, (20 * i) + 46, COLOR_WHITE);
                draw_string(tmpbuf, 25, 20 + (20 * i) + 5, blue);
            }
            else {
                draw_string(tmpbuf, 25, 20 + (20 * i) + 5, COLOR_WHITE);
            }
        }
		pthread_cond_signal(&fbChanged);
        pthread_cond_wait(&settingsChanged, &rendererLock);
#ifdef DEBUG
        printf("[menu] Settings changed\n");
#endif
    }
    pthread_mutex_unlock(&rendererLock);
}

/**
 * @brief Action for moving the red knob to the right
 */
void red_right() {
    if (rendered == MENU_INFO) {
        current_menu.selected++;
        if (current_menu.selected > current_menu.size - 1)
            current_menu.selected = 0;
        pthread_cond_signal(&settingsChanged);
    }
    else if (rendered == JULIA) {
        option_t opt = current_menu.options[2];
        *(double *)(opt.var) += opt.step;
        settings_commit();
    }
}

/**
 * @brief Action for moving the red knob to the left
 */
void red_left() {
    if (rendered == MENU_INFO) {
        current_menu.selected--;
        if (current_menu.selected < 0)
            current_menu.selected = current_menu.size - 1;
        pthread_cond_signal(&settingsChanged);
    }
    else if (rendered == JULIA) {
        option_t opt = current_menu.options[2];
        *(double *)(opt.var) -= opt.step;
        settings_commit();
    }
}

/**
 * @brief Action for moving the green knob to the right
 */
void green_right() {
    if (rendered == MENU_INFO) {
        option_t opt = current_menu.options[current_menu.selected];

        *(double *)(opt.var) += opt.step;

        if (*(double *)opt.var > opt.max_value)
            *(double *)(opt.var) = opt.max_value;
        else if (*(double *)(opt.var) < opt.min_value)
            *(double *)(opt.var) = opt.min_value;

        pthread_cond_signal(&settingsChanged);
    }
    else if (rendered == JULIA) {
        option_t opt = current_menu.options[3];
        *(double *)(opt.var) += opt.step;
        settings_commit();
    }
}

/**
 * @brief Action for moving the green knob to the left
 */
void green_left() {
    if (rendered == MENU_INFO) {
        option_t opt = current_menu.options[current_menu.selected];

        *(double *)(opt.var) -= opt.step;

        if (*(double *)opt.var > opt.max_value)
            *(double *)(opt.var) = opt.max_value;
        else if (*(double *)(opt.var) < opt.min_value)
            *(double *)(opt.var) = opt.min_value;

        pthread_cond_signal(&settingsChanged);
    }
    else if (rendered == JULIA) {
        option_t opt = current_menu.options[3];
        *(double *)(opt.var) -= opt.step;
        settings_commit();
    }
}

/**
 * @brief Action for clicking the red knob
 */
void red_click() {
    if (rendered == JULIA)
        rendered = JULIA_SLIDESHOW;
    else if (rendered == JULIA_SLIDESHOW)
        rendered = JULIA;
    settings_commit();
}

/**
 * @brief Action for clicking the green knob
 */
void green_click() {
    if (rendered == JULIA || rendered == JULIA_SLIDESHOW) {
        drawOverlay = !drawOverlay;
        pthread_cond_signal(&settingsChanged);
    }
}


/**
 * @brief Action for clicking the blue knob
 */
void blue_click() {
    swap_rendered();
}

/**
 * @brief Action for moving the blue knob to the left
 */
void blue_left() {
    if (rendered == JULIA) {
        option_t opt = current_menu.options[5];
        *(double *)(opt.var) -= opt.step;
        settings_commit();
    }
}

/**
 * @brief Action for moving the blue knob to the right
 */
void blue_right() {
    if (rendered == JULIA) {
        option_t opt = current_menu.options[5];
        *(double *)(opt.var) += opt.step;
        settings_commit();
    }
}